# JavaEE / SpringMVC boilerplate

Simple boilerplate base on an example from a random Spring book.

### Prerequisites

```
Java JDK 8
Maven 3.*

```

## Usage


Compile the source code 

```
mvn compile

```

Test the project (default: http://localhost:8080)

```
mvn jetty:run

```

Export the project to a war

```
mvn package

```
